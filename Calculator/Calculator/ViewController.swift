//
//  ViewController.swift
//  Calculator
//
//  Created by Chhaileng Peng on 6/2/18.
//  Copyright © 2018 Chhaileng Peng. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblNum: UILabel!
    
    @IBOutlet weak var lblOldNum: UILabel!
    
    @IBOutlet weak var btnClear: CustomButton!
    var userNum = "0"
    var oldNum = "0"
    var currentNum = "0"
    var currentSign = ""
    var nextSign = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func getNum(num: String){
        if userNum.count < 15 {
            if(userNum=="0"){
                userNum = num
                lblNum.text = String(userNum)
            }else{
                userNum += num
                lblNum.text =  String(userNum)
            }
            if userNum.count > 10 {
                lblNum.font = lblNum.font.withSize(30)
            }
        }
    }
    
    @IBAction func btnPosAndNega(_ sender: Any) {
        if checkFloatNum(numToCheck: userNum)=="0" {
            userNum = String(Int(userNum)! * (-1))
        }else{
            userNum = String(Float(userNum)! * (-1))
        }
        lblNum.text = userNum
    }
    @IBAction func btnClear(_ sender: Any) {
        lblNum.text = "0"
        userNum = "0"
        oldNum = "0"
        lblOldNum.text = ""
        currentNum = "0"
        currentSign = ""
        nextSign = ""
    }
    @IBAction func numPad1Click(_ sender: Any) {
        getNum(num: "1")
    }
    
    @IBAction func btnPointClick(_ sender: Any) {
        if(!userNum.contains(".")){
            userNum += "."
            lblNum.text = userNum
        }
    }
    
    @IBAction func btnZeroClick(_ sender: Any) {
        if(userNum != "0"){
            userNum += "0"
            lblNum.text = userNum
        }
    }
    
    @IBAction func btnTwoClick(_ sender: Any) {
        getNum(num: "2")
    }
    
    @IBAction func btnThreeClick(_ sender: Any) {
        getNum(num: "3")
    }
    
    @IBAction func btnFourClick(_ sender: Any) {
        getNum(num: "4")
    }
    
    @IBAction func btnFiveClick(_ sender: Any) {
        getNum(num: "5")
    }
    
    @IBAction func btnSixClick(_ sender: Any) {
        getNum(num: "6")
    }
    
    @IBAction func btnSevenClick(_ sender: Any) {
        getNum(num: "7")
    }
    
    @IBAction func btnEightClick(_ sender: Any) {
        getNum(num: "8")
    }
    
    @IBAction func btnNineClick(_ sender: Any) {
        getNum(num: "9")
    }
    
    func findFirstIndexOfString(text: String,char: String) -> Int {
        let range: Range<String.Index> = text.range(of: char)!
        let index: Int = text.distance(from: text.startIndex, to: range.lowerBound)
        return index
    }
    
    func checkFloatNum(numToCheck: String) -> String {
        var afterPointNum = ""
        var beforePointNum = ""
        if numToCheck.contains("."){
            var checkPoint = false
            for a in numToCheck{
                if(checkPoint==true){
                    afterPointNum += String(a)
                }
                if(a=="."){
                    checkPoint = true
                }
                if(checkPoint==false){
                    beforePointNum += String(a)
                }
                
            }
            if(afterPointNum.count==0){
                userNum = beforePointNum
                
                afterPointNum = "0"
            }
        }else{
            afterPointNum = "0"
        }
        return afterPointNum
    }
    
    
    @IBAction func btnPlusClick(_ sender: Any) {
        nextSign = "+"
        calculate(sign: "+",curSign: nil)
    }
    
    @IBAction func btnSubClick(_ sender: Any) {
        nextSign = "-"
        calculate(sign: "-",curSign: nil)
    }
    
    @IBAction func btnMultiClick(_ sender: Any) {
        nextSign = "x"
        calculate(sign: "x",curSign: nil)
    }
    
    @IBAction func btnDevClick(_ sender: Any) {
        nextSign = "/"
        calculate(sign: "/",curSign: nil)
    }
    
    @IBAction func btnModClick(_ sender: Any) {
        nextSign = "%"
        calculate(sign: "%", curSign: nil)
    }
    
    func calculate(sign:String,curSign:String?){
        
        if(currentSign==""){
            if sign=="+" {
                lblOldNum.text = userNum + " +"
                currentSign = "+"
            }
            else if sign=="-" {
                lblOldNum.text = userNum + " -"
                currentSign = "-"
            }
            else if sign=="x" {
                lblOldNum.text = userNum + " x"
                currentSign = "x"
            }
            else if sign=="/" {
                lblOldNum.text = userNum + " /"
                currentSign = "/"
            }else if sign=="%" {
                lblOldNum.text = userNum + " %"
                currentSign = "%"
            }
            oldNum = userNum
            userNum = "0"
            lblNum.text = userNum
        }else{
            if currentSign=="+" {
                oldNum = String(Float(userNum)! + Float(oldNum)!)
            }
            else if currentSign=="-" {
                oldNum = String((Float(oldNum)!) - (Float(userNum)!) )
            }
            else if currentSign=="x" {
                if(userNum != "0"){
                    oldNum = String((Float(oldNum)!) * (Float(userNum)!) )
                }
            }
            else if currentSign=="/" {
                if(userNum != "0"){
                    oldNum = String((Float(oldNum)!) / (Float(userNum)!) )
                }

            }
            else if currentSign=="%" {
                if(userNum != "0"){
                    oldNum = (String(Float(oldNum)!.truncatingRemainder(dividingBy: Float(userNum)!) ))
                }
            }
            oldNum = checkResult(checkNum: Float(oldNum)!)
            lblOldNum.text = oldNum + " " + nextSign
            userNum = "0"
            lblNum.text = userNum
            currentSign = nextSign
        }
    }
    
    func checkResult(checkNum:Float) -> String {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ""
        formatter.decimalSeparator = "."
        formatter.numberStyle = .decimal
        let str = formatter.string(from: checkNum as NSNumber) ?? ""
        return str
    }
    
    @IBAction func btnEqualClick(_ sender: Any) {
        var tempUserOldNum = oldNum
        var tempUserNum = userNum
        let tempSign = currentSign
        calculate(sign: "", curSign: "")
        let tempTotal = oldNum
        btnClear.sendActions(for: .touchUpInside)
        if tempUserOldNum.contains("-") {
            tempUserOldNum = "(" + tempUserOldNum + ")"
        }
        if tempUserNum.contains("-"){
            tempUserNum = "(" + tempUserNum + ")"
        }
        lblOldNum.text = tempUserOldNum + " \(tempSign) " + tempUserNum
        userNum = tempTotal
        lblNum.text = userNum
    }
}

